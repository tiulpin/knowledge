---
description: Nothing going on here for now.
---

# 🗣 Talks

## 20/11/2019: Kaggle Experience

My small talk on a small Kaggle computer vision experience: two silver medals \(in Russian\).

Slides: [**PDF**](https://tiulp.in/pdf/20_11_kaggle_detection.pdf)

{% embed url="https://www.youtube.com/embed/eCRYgb6xSd8" %}

## xx/xx/20xx: TBA

Waiting...

