---
description: 'People used this list: Me.'
---

# 👀 Wishlist

## Furniture

{% embed url="https://www.ikea.com/ru/ru/p/dagotto-podstavka-dlya-nog-chernyy-90378903/" %}

## Books & Posters

{% embed url="https://www.redbubble.com/people/immortalloom/works/22929408-official-big-o-cheat-sheet-poster?SSAID=389818&finish=semi\_gloss&p=poster&size=large&sscid=21k4\_c5cjc&utm\_campaign=banner&utm\_medium=affiliates&utm\_source=shareasale" %}

{% embed url="https://society6.com/product/the-neural-network-zoo2036937\_print?sku=s6-10900133p4a1v45" %}

{% embed url="https://www.mann-ivanov-ferber.ru/books/mif/050/" %}

{% embed url="https://www.corpus.ru/products/asja-kazanceva-kto-by-mog-podumat.html" %}

## Apps & Subscriptions

{% embed url="https://appshopper.com/user/tiulpin/wishlist/0" %}

## Food & Drinks

{% embed url="https://store.artlebedev.ru/coffee-shop/tea/tea-capsules-jasmine/" %}

## Gadgets

{% embed url="https://www.logitech.com/en-us/product/mx-master-3" %}

{% embed url="http://en.obins.net/anne-pro2" %}

{% embed url="https://www.yubico.com/product/yubikey-5-nfc" %}

{% embed url="https://www.apple.com/airpods-pro/" %}

{% embed url="https://market.yandex.ru/product--robot-pylesos-roborock-sweep-one/1806585982/" %}

{% embed url="https://www.apple.com/ipad-pro/" %}

{% embed url="https://www.apple.com/apple-pencil/" %}



## Accessories & Bags

{% embed url="https://www.aersf.com/day-pack-2-black" %}

{% embed url="https://www.feelosophy.ru/product-page/%D0%BC%D0%B5%D1%82%D0%B0%D0%BB%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9-%D0%B7%D0%BD%D0%B0%D1%87%D0%BE%D0%BA-%D1%81%D0%B2%D0%BE%D0%B1%D0%BE%D0%B4%D0%BD%D1%8B%D0%B9-%D0%BE%D1%82-%D0%B7%D0%B0%D0%B1%D0%BE%D1%82" %}

{% embed url="https://www.leatherman.com/tread-metric-488.html" %}

## Clothes

{% embed url="https://shop.planetargon.com/collections/oh-my-zsh/products/ohmyzsh-ascii-shirt?variant=27216009345" %}

{% embed url="https://www.ebay.com/itm/Gildan-Softstyle-Official-Vim-Logo-Vi-IMproved-Script-T-Shirt/112817736531" %}

{% embed url="https://shop.protonmail.com/products/coming-soon-dark-t-shirt" %}

{% embed url="https://shop.protonmail.com/products/protonmail-man-zipped-hoodie" %}

{% embed url="https://shop.dev.to/collections/2018-new-merch/products/dev-unisex-t-shirt" %}

