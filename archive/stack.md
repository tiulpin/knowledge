---
description: Archive for some tiulp.in posts.
---

# 💻 Stack

## Bitwarden

### Why do you need a password manager?

It's 2019 and we still heavily rely on passwords. Strong passwords. Not reusable ones. If you aren't a [Memory Championship](https://en.wikipedia.org/wiki/World_Memory_Championships) competitor, I'm sure you won't be able to remember all of your passwords. So here is the best solution for this security problem: use a password manager.

![](http://giphygifs.s3.amazonaws.com/media/1I2NwmjvSzNS0/giphy.gif)

#### One password to type them all

All you would need is to remember one secret phrase. Isn't it nice? But that's not enough -- you should choose the perfect password manager for your vault. What are my requirements for a good password manager? Here they are:

* Ability to sync passwords among my devices -- most likely you have more than one device, so you should have access to your logins on every platform.
* Security audits -- it's essential to have expert reports about your password manager to decide one thing for yourself: trust or not to trust.
* Open source code -- you are going to put your passwords there, how can it be trusted, if it's not open-sourced?

![](http://giphygifs.s3.amazonaws.com/media/smW5FBep69d3q/giphy.gif)

### Good news, everyone! Bitwarden!

So I found the ultimate password manager -- Bitwarden: it's an open-source project, it's modern, and this password manager has nice apps for every OS: you are free to use desktop apps \(Windows / Linux / macOS\), browser plugins \(Firefox, Chrome, Safari, Edge...\) and mobile apps \(iOS / Android\), you can even import your passwords from any popular password manager. Check their website out -- [https://bitwarden.com/](https://bitwarden.com/)

#### Self-host your Bitwarden server

[bitwarden\_rs](https://github.com/dani-garcia/bitwarden_rs) is an unofficial Bitwarden compatible server written in Rust. Comparing to the official server implementation, this server would be more lightweight and not so heavy on your server RAM. The full implementation of Bitwarden API is provided, so you can use this server app without any doubt.

Refer to [bitwarden\_rs wiki](https://github.com/dani-garcia/bitwarden_rs/wiki) for the installation guide. I could not run this docker image on the default HTTP port, so I set another port for my container and set up a reverse proxy using Nginx web server. Here is an example Nginx reverse proxy configuration: my docker container port is `8888`, Nginx proxy and container are located on the same machine, so I can use `localhost` address to point to Bitwarden docker.

```text
server {
 listen 443 ssl http2;
 server_name bitwarden.*;

 # Specify SSL config if using a shared one.
 #include conf.d/ssl/ssl.conf;

 # Allow large attachments
 client_max_body_size 128M;

 location / {
 proxy_pass http://localhost:8888;
 proxy_set_header Host $host;
 proxy_set_header X-Real-IP $remote_addr;
 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 proxy_set_header X-Forwarded-Proto $scheme;
 }

 location /notifications/hub {
 proxy_pass http://localhost:3012;
 proxy_set_header Upgrade $http_upgrade;
 proxy_set_header Connection "upgrade";
 }

 location /notifications/hub/negotiate {
 proxy_pass http://localhost:8888;
 }
}
```

...and don't forget to set your SSL certificates. Now you can proceed to the login page of your new password vault.

![](https://media.giphy.com/media/ZiK1I0ceU2FMc/giphy.gif)

Stay safe, don't reuse, forget or give away \(to someone you don't want to\) your passwords.

## Overleaf

Are there any good LaTeX editors for Windows, Linux? I thought there are not.

![](https://media1.giphy.com/media/6uGhT1O4sxpi8/giphy.gif)

But then I found Overleaf. From this moment on, I did not experience any trouble with writing in LaTeX. Here are 4 reasons why you should consider switching to Overleaf.

### Documentation

![](https://media.giphy.com/media/VXbCTL9B3h8xq/giphy.gif)

Overleaf has rich LaTeX documentation, located here: [https://www.overleaf.com/learn](https://www.overleaf.com/learn)

It's about LaTeX, it's about their editor. There are many good examples on how to use LaTeX even if you are not an Overleaf user, so don't forget to bookmark the page :\)

### Collaboration

![](https://media3.giphy.com/media/I1cQKEBWmRgkM/giphy.gif)

You can edit documents with your coworkers and forget about committing and pushing to your git repo. Change. Review. Restore any previous versions. All of those things are possible here, in Overleaf.

![](/assets/images/overleaf/changes.png)

Aaaand there is a chat!

### Control version

![](https://media.giphy.com/media/3otPorWLQJq5GmHRtu/giphy.gif)

Even if you are happy with Overleaf collaboration tools, you may need integration with your git repository. There is a GitHub sync feature available for every your Overleaf project.

![](/assets/images/overleaf/github.png)

### Publication

![](https://media.giphy.com/media/Ua48yqUY0x5uM/giphy.gif)

So you finally completed your document. Or maybe your new paper? There is a way to publish it using Overleaf!

![](/assets/images/overleaf/publication.png)

### Vincent, we happy?

![](https://media.giphy.com/media/l2YWjo0mz9WcDFiW4/giphy.gif)

You can register your Overleaf account using my referral link to give me some bonuses:

[https://www.overleaf.com?r=8cb3a710&rm=d&rs=b](https://www.overleaf.com?r=8cb3a710&rm=d&rs=b)

