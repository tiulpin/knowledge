---
description: >-
  I have a friend who constantly asks me if I have a referral link for some
  service. This page is for him. But you can use the provided links too :)
---

# 🔗 Referral links

## Language Learning — Duolingo

{% embed url="https://invite.duolingo.com/BDHTZTB5CWWKTTNK7YG4KWC6EU" %}

## LaTeX Editor — Overleaf

There is no better LaTeX editor \(email me in case you know better\). I am an Overleaf Advisor and I use Overleaf almost every day.

{% embed url="https://www.overleaf.com/?r=8cb3a710&rm=d&rs=b" %}

## Cloud Computing — Spell

{% embed url="https://web.spell.run/refer/tiulpin" %}

## OCR → LaTeX — Mathpix Snip

{% embed url="https://accounts.mathpix.com/signup?referral\_code=8Xk3VYKJYv" %}

##  🇷🇺 в России

### Банк

{% embed url="https://www.tinkoff.ru/sl/Q98F3BvYya" %}

### Сотовая связь

{% embed url="https://www.tinkoff.ru/sl/5Z42JfmqzUb" %}

### Такси и каршеринг

{% embed url="https://6lg6.adj.st/promo/refqgjwK5?adjust\_t=ooi3gfn&adjust\_deeplink=yandexdrive%3A%2F%2Fpromo%2F%0ArefqgjwK5" %}

{% embed url="https://trk.mail.ru/c/ho1663?mt\_sub1=CNJ64ZP" %}

{% embed url="https://invite.bolt.eu/Y5V2TZ" %}

  


