---
description: 'When all you have is a hummer, everything looks like a nail.'
---

# 👋 Hello, Friend \(World\)

This is my personal knowledge base. 

Some of the notes are written in Russian, but the most important stuff is in English.

There is a GitHub repo for that, you can make a pull request, if you want 🙃

{% embed url="https://github.com/tiulpin/kb" %}

## Why?

I needed a solution to store some of my notes using open-source solutions. 

Thanks to [GitBook ](https://www.gitbook.com/)for that fancy page you see.

