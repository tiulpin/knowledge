# Table of contents

* [👋 Hello, Friend \(World\)](README.md)
* [🎓 Computer Science Chapter](csc/README.md)
  * [🧮 Algebra  \[WIP\]](csc/algebra.md)
  * [🥼 Calculus \[WIP\]](csc/calculus.md)
  * [🧪 Probability Theory](csc/proba.md)
  * [✖️ Discrete Math and Logic  \[WIP\]](csc/discrete-math.md)
  * [🦾 Algorithms  \[WIP\]](csc/algorithms.md)
  * [🔗 Data Structures  \[WIP\]](csc/data-structures.md)
  * [🐍 Python \[WIP\]](csc/python-wip.md)
  * [👾 DevOps \[WIP\]](csc/devops-wip.md)
  * [📖 Machine Learning  \[WIP\]](csc/ml/README.md)
    * [Classification](csc/ml/classification/README.md)
      * [Algorithms](csc/ml/classification/algorithms.md)
      * [Metrics](csc/ml/classification/metrics.md)
    * [Regression](csc/ml/regression/README.md)
      * [Algorithms](csc/ml/regression/algorithms.md)
      * [Metrics](csc/ml/regression/metrics.md)
    * [Recommendations](csc/ml/recommendations/README.md)
      * [Matrix decompositions for explicit / implicit feedback](csc/ml/recommendations/algorithms.md)
      * [Metrics](csc/ml/recommendations/metrics.md)
* [📝 Personal archive](archive/README.md)
  * [🕸 Сообщества](archive/soobshestva.md)
  * [🗣 Talks](archive/talks.md)
  * [🔗 Referral links](archive/referral-links.md)
  * [👀 Wishlist](archive/wishlist.md)
  * [💻 Stack](archive/stack.md)
  * [👁‍🗨 NoCode examples](archive/nocode-examples.md)

