---
description: Dreamer's Ball
---

# 📖 Machine Learning  \[WIP\]

{% hint style="success" %}
A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P, if its performance at tasks in T, as measured by P, improves with experience E.
{% endhint %}

{% page-ref page="classification/" %}

{% page-ref page="regression/" %}

{% page-ref page="recommendations/" %}



