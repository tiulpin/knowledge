# Metrics

## precision@k, recall@k

With such map we can get a new definitions for precision and recall:

| Binary classifier | Recommender system |
| :--- | :--- |
| with condition | of all the possible relevant \("correct"\) items for a user |
| predicted positive | of items we recommended \(we predict these items as "relevant"\) |
| correct positives | of our recommendations that are relevant |

$$
\textrm{RecSys Precision:} \qquad P = \frac{\textrm{# of our recommendations that are relevant}}{\textrm{# of items we recommended}}
$$

$$
\textrm{RecSys Recall:} \qquad r  = \frac{\textrm{# of our recommendations that are relevant}}{\textrm{# of all the possible relevant items}}
$$

Imagine taking your list of N recommendations and considering only the first element, then only the first two, then only the first three, etc... these subsets can be indexed by k. **Precision and Recall at cutoff k, P\(k\) and r\(k\), are simply the precision and recall calculated by considering only the subset of your recommendations from rank 1 through k.** Really it would be more intuitive to say "up to cutoff k" rather than "at".

![the bank example](../../../.gitbook/assets/image%20%284%29.png)

## Average Precision <a id="Average-Precision"></a>

OK are you ready for Average Precision now? If we are asked to recommend N items, the number of relevant items in the full space of items is m, then:

$$
\textrm{AP@}N = \frac{1}{m}\sum_{k=1}^N \textrm{($P(k)$ if $k^{th}$ item was relevant)} = \frac{1}{m}\sum_{k=1}^N P(k)\cdot rel(k),
$$

where $$\textrm{rel}(k)$$ is just an indicator that says whether that kth item was relevant \($$\textrm{rel}(k)=1$$\) or not \($$\textrm{rel}(k)=0$$\). I'd like to point out that instead of recommending N items would could have recommended, say, $$2N$$, but the $$\textrm{AP}@N$$ metric says we only care about the average precision up to the $$N$$th item.

$$
\textrm{AP@}N = \sum_{k=1}^N \textrm{(precision at $k$)}\cdot\textrm{(change in recall at $k$)} = \sum_{k=1}^N P(k)\Delta r(k),
$$

## MAP@k

OK that was Average Precision, which applies to a single data point \(like a single user\). What about MAP@N? All that remains is to average the AP@N metric over all your \|U\| users. Yes, an average of an average.

$$
\textrm{MAP@N} = \frac{1}{|U|}\sum_{u=1}^|U|(\textrm{AP@N})_u = \frac{1}{|U|} \sum_{u=1}^|U| \frac{1}{m}\sum_{k=1}^N P_u(k)\cdot rel_u(k).
$$

## ndcg@k

