# Algorithms

## Logistic Regression

$$
h(\theta) = \frac{1}{1 + e^{-(\theta_0 + \theta_1x + \theta_2x \dots )}}
$$

$$
J(\theta) = \frac{1}{m} \cdot (- y^{T} \log(h) - (1 - y)^T \log(1 - h))
$$

### Hyperparameters

* $$ \alpha $$, learning rate
* $$ \lambda $$, regularization parameter

### Pros and Cons

#### Pros

* easy, fast and simple classification method.
* θ parameters explains the direction and intensity of significance of independent variables over the dependent variable == feature importance
* loss function is always convex

#### Cons

* can't be applied on non-linear classification problems
* requires proper selection of features
* good signal to noise ratio is expected
* colinearity and outliers tampers the accuracy of the model

## Decision Tree

Decision tree is the structure that is branched off from a homogeneous probability distributed root node to highly heterogeneous leaf nodes, for deriving the output. 

Regression trees are used for dependent variable with continuous values and classification trees are used for dependent variable with discrete values.

A recursive, greedy based algorithm is used to derive the tree structure.

**CART algorithm**: uses Gini index as the metric to measure information 

$$
G = 1 - \sum_{i=1}^C p^2_t
$$

```text
# Gini Index:
for each branch in split:
    Calculate percent branch represents # used for weighting
    for each class in branch:
        Calculate probability of class in the given branch.
        Square the class probability.
    Sum the squared class probabilities.
    Subtract the sum from 1. # This is the Ginin Index for branch
Weight each branch based on the baseline probability.
Sum the weighted gini index for each split.
```

### Hyperparameters

* **criterion,** cost function for selecting the next tree node. 
* **max depth,** the maximum allowed depth of the decision tree.
* **minimum samples split,** is the minimum nodes required to split an internal node.
* **minimum samples leaf,** minimum samples that are required to be at the leaf node.
* ...

### Pros and Cons

#### Pros

* needs no preprocessing on data
* no assumptions on distribution of data
* handles colinearity efficiently.
* provides understandable explanation over the prediction

#### Cons

* overfits easily, pruning can be used
* is prone to outliers
* tree may grow to be very complex while training complicated datasets
* looses valuable information while handling continuous variables

## Random Forest

Random Forest have a set of decision trees ensembled with bagging to obtain classification \(and regression\) outputs. 

**Classification:** calculates the output using majority voting.

### Hyperparameters

* **n\_estimators,** the number of trees in the forest. With large number of trees comes high accuracy, but high computational complexity.
* **maximum features,** the ****maximum number of features allowed in an individual tree
* **minimum sample leaf,** the minimum number of samples required to split an internal node

### Pros and Cons

#### Pros

* is accurate and powerful
* handles overfitting efficiently
* supports implicit feature selection and derives feature importance

#### Cons

* computationally complex and slower when forest becomes large
* is not a well descriptive model over the prediction

## Gradient Boosting Machine





