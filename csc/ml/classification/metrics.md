---
description: 'Quality, quality, quality for developers, developers, developers.'
---

# Metrics

## Accuracy

Overall performance of model.

* Easy to **explain** to non-technical people.
* Good when your problem is **balanced**.
* Good when every class is **equally important**.

```text
from sklearn.metrics import confusion_matrix

y_pred_class = y_pred_pos > threshold 
tn, fp, fn, tp = confusion_matrix(y_true, y_pred_class).ravel()

accuracy = (tp + tn) / (tp + fp + fn + tn)
```

## ROC AUC

The receiver operating curve, also noted ROC, is the plot of TPR versus FPR by varying the threshold. AUC is Area Under that Curve.

* **Good** when you ultimately **care about ranking predictions** and not necessarily about outputting well-calibrated probabilities.
* **Good** when you **care equally about positive and negative classes**. 
* **??? Bad** when your **data is heavily imbalanced**. 

### True Positive Rate and False Positive Rate

```text
from sklearn.metrics import confusion_matrix

y_pred_class = y_pred_pos > threshold
tn, fp, fn, tp = confusion_matrix(y_true, y_pred_class).ravel()

tpr = tp / (fn + tp)
fpr = fp / (fp + tn)
```

```text
# call it from scikit-learn, I'm too lazy to implement it here
```

An extensive discussion of ROC Curve and ROC AUC score can be found in this [article by Tom Fawcett](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.10.9777&rep=rep1&type=pdf).

## PR AUC

Precision-Recall curve, also noted PR, is the plot of Precision versus Recall by varying the threshold. AUC is Area Under that Curve.

* **Good** when you want to **communicate precision/recall decision** to other stakeholders.
* **Good** when you want to **choose the threshold that fits the business problem**.
* **Good** when your data is **heavily imbalanced**. 
* **Good** when **you care more about positive than negative class**. 

### Precision and Recall

Precision: How accurate the positive predictions are.

Recall: Coverage of actual positive sample.

```text
from sklearn.metrics import confusion_matrix

y_pred_class = y_pred_pos > threshold
tn, fp, fn, tp = confusion_matrix(y_true, y_pred_class).ravel()

precision = tp / (tp + fp)
recall = tp / (tp + fn)
```

```text
# call it from scikit-learn, I'm too lazy to implement it here
```

## F-Score

With **0 &lt; beta &lt; 1** we care more about **precision** and so the higher the threshold the higher the F beta score. When **beta &gt; 1** our optimal threshold moves toward lower thresholds and with beta = 1 it is somewhere in the middle.

* When you care more about the **positive class**.
* It **can be easily explained to business stakeholders** which in many cases can be a deciding factor.

```text
from sklearn.metrics import confusion_matrix

y_pred_class = y_pred_pos > threshold
tn, fp, fn, tp = confusion_matrix(y_true, y_pred_class).ravel()

recall = tp / (tp + fn)
precision = tp / (tp + fp)

f_b_score = ((1 + b**2) * precision * recall) / (b**2 * precision + recall)
```

