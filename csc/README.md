# 🎓 Basic Computer Science Chapter

Can I become who I want to be?

That's a tough question but thankfully, our team is on it. Please bear with us while we're investigating.

## Have you had a chance to answer the previous question?

Yes, after a few months we finally found the answer. Sadly, Viktor is on vacations right now so I'm afraid we are not able to provide the answer at this point.

## Contents

{% page-ref page="proba.md" %}

## References

{% embed url="https://www.bigocheatsheet.com/" %}

{% embed url="https://stanford.edu/~shervine/teaching/" %}

{% embed url="https://neptune.ai/blog" %}

{% embed url="http://sdsawtelle.github.io/blog/output/mean-average-precision-MAP-for-recommender-systems.html" %}



