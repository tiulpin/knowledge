# 🥼 Calculus \[WIP\]

## Books \(in Russian\)

{% embed url="https://www.bookvoed.ru/book?id=378670" %}

## 

{% hint style="warning" %}
**\#TODO:** предел, непрерывность, производная, первообразная, дифференциал, нахождение экстремума функции от одной и от многих переменных, формула Тейлора.
{% endhint %}

{% hint style="success" %}
**@3blue1brown** can explain anything even if you understand nothing
{% endhint %}

{% embed url="https://www.youtube.com/playlist?list=PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr" caption="The Essence of Calculus" %}

