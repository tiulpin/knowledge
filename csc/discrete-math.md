# ✖️ Discrete Math and Logic  \[WIP\]

## Binary Relations

{% hint style="warning" %}
**\#TODO**: отображения и отношения и их свойства, транзитивное замыкание отношения, эквивалентность, отношения порядка
{% endhint %}

## Logic

{% hint style="warning" %}
**\#TODO**: логика высказываний, кванторы, метод математической индукции
{% endhint %}

## Graph Theory

{% hint style="warning" %}
**\#TODO**: основные понятия теории графов, лемма о рукопожатиях, критерий двудольности, оценки числа ребер, характеризация деревьев.
{% endhint %}



