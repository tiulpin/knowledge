# 🧪 Probability Theory

## Books \(in Russian\)

{% embed url="https://www.bookvoed.ru/book?id=343790" %}

## Probability of an event

Any subset $$E$$ of the sample space is known as **an event.** 

That is, an event is a set consisting of possible outcomes of the experiment. If the outcome of the experiment is contained in $$E$$, then we say that $$E$$ __has occurred.

### **Axioms of probability**

For each event $$E$$, we denote $$\operatorname{P}(E)$$ as the probability of event $$E$$ occurring.

![](../.gitbook/assets/image%20%285%29.png)

The probability that at least one of the elementary events in the sample space will occur is $$1$$.

![](../.gitbook/assets/image%20%287%29.png)

Every probability value is between $$0$$ and $$1$$ included.

![](../.gitbook/assets/image%20%289%29.png)

For any sequence of mutually exclusive events$$E_i$$we have:

$$
\operatorname{P} \left( \bigcup_{i=1}^n E_i \right) = \sum_{i = 1}^n \operatorname{P} (E_i)
$$

### **Combinatorics**

**A permutation** is an arrangement of$$k$$objects from a pool of$$n$$objects, in a given order. The number of such arrangements is:

$$
P(n, k) = \frac{n!}{(n - k)!}
$$

**A combination** is an arrangement of$$k$$objects from a pool of$$n$$objects, where the order does not matter. The number of such arrangements is:

$$
C(n, k) = \frac{n!}{k! \cdot (n - k)!}
$$

{% hint style="info" %}
 We note that for $$0 \le k \le n$$ _,_ we have$$P(n, r) \ge C(n, r)$$ 
{% endhint %}

### **Conditional probabilities**

**Conditional probability** is the **probability** of one event occurring with some relationship to one or more other events.

**Independence.** Two events$$A$$and$$B$$are independent if and only if we have:

$$
\operatorname{P} (A \cap B) = \operatorname{P}(A) \cdot \operatorname{P}(B)
$$

**Law of total probability.** Given an event$$A$$, with known conditional probabilities given any of the $$B_i$$ events, each with a known probability itself, what is the total probability that$$A$$will happen? The answer is

$$
\operatorname{P}(A) = \sum_{i = 1}^n \operatorname{P}(A | B_i) \cdot \operatorname{P}(B_i)
$$

**Bayes' rule.** For events$$A$$and$$B$$such that$$\operatorname{P}(B) > 0$$, we have

$$
\operatorname{P} (A | B) = \frac{\operatorname{P}(B | A) \cdot \operatorname{P}(A)}{\operatorname{P}(B)}
$$

## Expectation and Moments of the Distribution

The formulas will be explicitly detailed for the discrete **\(D\)** and continuous **\(C\)** cases.

**Expected value.** The expected value of a random variable, also known as the mean value or the first moment, is often noted $$\operatorname{E}[X]$$and is the value that we would obtain by averaging the results of the experiment infinitely many times.

$$
\textbf{(D) } \operatorname{E} [X] = \sum_{i  = 1}^n x_i \cdot f(x_i) \hspace{5em} \textbf{(C) } \operatorname{E} [X]= \int_{-\infty}^{+\infty} x \cdot f(x) \ dx
$$

**Variance.** The variance of a random variable, often noted $$\operatorname{Var}[X]$$, is a measure of the spread of its distribution function.

$$
\operatorname{Var}(X) = \operatorname{E}[(X −\operatorname{E}[X])^2] = \operatorname{E}[X^2] − \operatorname{E}[X]^2
$$

## Some Inequalities

**Markov's inequality.** Let$$X$$be a random variable and$$a > 0$$

$$
\operatorname{P} (X\geq a)\leq {\frac {\operatorname {E} (X)}{a}}
$$

**Chebyshev's inequality**. Let$$X$$be a random variable with expected value$$\mu$$,  

$$
\operatorname{P} (| X - \mu| \ge k \sigma) \leq \frac{1}{k^2}
$$

