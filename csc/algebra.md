# 🧮 Algebra  \[WIP\]

## Algebric Structures

![https://github.com/mavam/abstract-algebra-cheatsheet](../.gitbook/assets/image%20%281%29.png)

{% hint style="warning" %}
**\#TODO:** факторизация, идеал, сравнения, алгоритм Евклида, теоремы Эйлера и Ферма, кольцо многочленов, число корней многочлена
{% endhint %}

## Linear Algebra

{% hint style="warning" %}
**\#TODO:** линейные пространства и операторы, базис, размерность, ранг, собственные числа и собственные векторы, характеристический многочлен.
{% endhint %}

{% hint style="success" %}
**@3blue1brown** can explain anything even if you understand nothing
{% endhint %}

{% embed url="https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE\_ab" caption="The Essence of Linear Algebra" %}



